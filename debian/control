Source: utidylib
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Michal Čihař <nijel@debian.org>
Build-Depends:
    debhelper (>= 9),
    dh-python,
    libtidy5.6 | libtidy5,
    python-six,
    python-setuptools,
    python3-all,
    python3-six,
    python3-setuptools
Build-Depends-Indep: python-all (>= 2.6.6-3)
Standards-Version: 3.9.8
Homepage: https://cihar.com/software/utidylib/
Vcs-Git: https://salsa.debian.org/python-team/modules/utidylib.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/utidylib

Package: python-utidylib
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}, python-six, libtidy5.6 | libtidy5 | libtidy-0.99-0 (>= 20051018)
Provides: ${python:Provides}
Breaks: ${python:Breaks}
Description: Python wrapper for TidyLib
 Corrects markup in a way compliant with the latest standards, and
 optimal for the popular browsers.  It has a comprehensive knowledge
 of the attributes defined in the HTML 4.0 recommendation from W3C,
 and understands the US ASCII, ISO Latin-1, UTF-8 and the ISO 2022
 family of 7-bit encodings.  In the output:
 .
  * HTML entity names for characters are used when appropriate.
  * Missing attribute quotes are added, and mismatched quotes found.
  * Tags lacking a terminating '>' are spotted.
  * Proprietary elements are recognized and reported as such.
  * The page is reformatted, from a choice of indentation styles.
 .
 This package contains uTidylib, a Python 2 wrapper for TidyLib.

Package: python3-utidylib
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-six, libtidy5.6 | libtidy5 | libtidy-0.99-0 (>= 20051018)
Provides: ${python3:Provides}
Breaks: ${python3:Breaks}
Description: Python wrapper for TidyLib
 Corrects markup in a way compliant with the latest standards, and
 optimal for the popular browsers.  It has a comprehensive knowledge
 of the attributes defined in the HTML 4.0 recommendation from W3C,
 and understands the US ASCII, ISO Latin-1, UTF-8 and the ISO 2022
 family of 7-bit encodings.  In the output:
 .
  * HTML entity names for characters are used when appropriate.
  * Missing attribute quotes are added, and mismatched quotes found.
  * Tags lacking a terminating '>' are spotted.
  * Proprietary elements are recognized and reported as such.
  * The page is reformatted, from a choice of indentation styles.
 .
 This package contains uTidylib, a Python 3 wrapper for TidyLib.
