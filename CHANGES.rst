Changes
=======

0.4
---

* Compatibility with html-tidy 5.6.0.
* Added support for Python 3.

0.3
---

* Initial release under new maintainer.
* Incorporated Debian patches.
* Various compatiblity fixes (eg. with 64-bit machines).
* Various code cleanups.
* New test suite.
* New documentation.
* Support for new HTML 5 tidy library.
